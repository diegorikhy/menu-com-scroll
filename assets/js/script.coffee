menu =
  w:             $(window)
  topScroll:     60
  position:      60
  oddScroll:     0
  initScroll:    true
  topMenu:       $('#top-menu')
  topBar:        $('#top-bar')
  topBarContent: $('#top-bar-content')
  init: ->
    @setTopBarHeight()
  setTopBarHeight: ->
    @topBar.css 'height', @topBarContent[0].offsetHeight
  scroll: ->
    scroll = @w.scrollTop()
    @oddScroll = scroll if @initScroll
    @initScroll = false

    @position += (@oddScroll - scroll)
    @position = Math.min(@position, @topScroll)
    @position = Math.max(@position, 0)

    if scroll > @topScroll
      @topBarContent.addClass 'fixed'
    else if scroll < 20
      @topBarContent.removeClass 'fixed'

    @topMenu.css 'top', @position - @topScroll
    @topBarContent.css 'top', @position
    @oddScroll = scroll

menu.init()
menu.w.scroll -> menu.scroll()
menu.w.resize -> menu.setTopBarHeight()
